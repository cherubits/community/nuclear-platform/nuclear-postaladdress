from django import forms
from postaladdress.forms import AddressField


class PersonForm(forms.Form):
    address = AddressField()
